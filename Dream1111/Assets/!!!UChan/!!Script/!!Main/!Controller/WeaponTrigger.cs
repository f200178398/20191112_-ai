﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class WeaponTrigger : MonoBehaviour
    {
        //****用 來控制武器的Collider開關後與敵人的觸發


        public bool attackTri;
        public FightSystem fightSy;
        public Collider col;

        private void Awake()
        {
            fightSy = GameObject.Find("FightSystem"). GetComponent<FightSystem>();
        }
        private void Start()
        {
            
        }
        private void Update()
        {
            
        }
        

        public void OnTriggerEnter(Collider target) //判定武器是否接觸到敵人
        {
            //attackTri = true;

            if (target.gameObject.tag == "Enemy"  /*&& StateManager.Instance().rt*/)
            {
                //attackObj = target.gameObject;
                attackTri = true;
                fightSy.Attackking(); //攻擊  確實執行
               
                //Destroy(target.gameObject);  // 可確實執行
                //Destroy(target);//可確實執行 刪除敵方COL
                //print("武器打到敵人");//確實執行
                //print(attackTri); //確實執行
            }
            else
            {
                attackTri = false;
            }
        }
        public void OnTriggerExit(Collider target) // 
        {
            if (target.gameObject.tag == "Enemy")  // 如果目標不是敵人, 則不會觸發武器判定
            {
                //attackObj = null;
                attackTri = false;
            }
        }
    }

}
