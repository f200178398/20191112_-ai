﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class EnemyTarget : MonoBehaviour
    {
        AI_data ai_data;
        AI_States ai_states;
        FightSystem fightSystem;
        WeaponTrigger weaponTrigger;
        public float maxhp;
        public float currentHp;
        
        public bool takeDamege;
        public bool machineDamage;
        public bool attack;
        Animator anim;

        public float distance;
        public GameObject controller;
        public bool isdead;

        public float enemyDamage_Melle;

        public bool isTiming;
        public float currentTime;

        private void Awake()
        {
            ai_data = gameObject.GetComponent<AI_data>(); //從AI_data 要血量資料
            ai_states = gameObject.GetComponent<AI_States>();
            
            
           
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>(); //從戰鬥系統上要攻擊指令判定 和 主角傷害
            weaponTrigger = GameObject.Find("Weapon_point").GetComponent<WeaponTrigger>(); //從武器觸發系統上 檢視 有無觸發攻擊事件
        }

        void Start()
        {
            ai_data.Target = controller;//把EnemyTarget裡的controller傳到AI_data裡的Target當目標 AI要針對玩家(Target
           
            
            maxhp = ai_data.maxHp; //遊戲開始的最大血量
            currentHp = maxhp;
            anim = GetComponent<Animator>();
            enemyDamage_Melle = ai_data.melleAttackD_Damage; // 先取得此敵人的進佔傷害
            //Debug.Log(maxhp);
            //Debug.Log(fightSystem);
            // weaponTrigger.attackTri = takeDamege;
            // print(fightSystem.damage);  //確認主角的初始傷害   可以在戰鬥系統面板修改
            print(takeDamege);
        }

        // Update is called once per frame
        void Update()
        {
           print(ai_data.enemy+"目標是"+ ai_data.Target);
            //print(ai_data.float_EnemyMaxRotateSpeed);
            // print(ai_data.enemy);
            AIFunction.Seek(ai_data);
            AIFunction.Move(ai_data);//移動

           // print("玩家有抓到" + ai_data.Target);
            //weaponTrigger.attackTri = takeDamege; //這段似乎沒執行
           // DistanceForAttack();
            //TakeDamage();
           // Dead();
            //MachineDamage(); //未完成 造成敵人全體一職扣寫
           // EnemyAttack(); //攻擊主角  必須將方法改在
            //melleattack();

            // print(fightSystem.enemyObj);// 確實抓到目標
            //takeDamege = false;
            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    currentHp -= 100f;
            //    print(currentHp);
            //    dead();
            //}

        }
        public bool Dead()
        {
            bool die = true;
            if(currentHp <= 0)
            {
                
                currentHp = 0;
                print("dead!");
                anim.SetBool("dead", true);
                this.enabled = false;
                Destroy(gameObject,5f);
                isdead = true;
            }
            return die;
        }
        public void TakeDamage() //成受傷害  //已經正確觸發
        {
            
            if(takeDamege == true)
            {
                print("擊中 敵人 次數");
                //currentHp -= 100f; //造成傷害
                currentHp -= fightSystem.damage; //造成傷害
                anim.SetBool("take damage",true);
                print(currentHp);
               
            }
            else
            {
                anim.SetBool("take damage", false);
            }

        }
       
        public void MachineDamage() //護衛機的傷害
        {
            if(machineDamage == true  )  //BUG 按著CTRL 會持續扣寫
            {
            currentHp -= fightSystem.machineDamage;
            print(currentHp);
                anim.SetBool("take damage", true);   //已經跟TakeDamage 重複的CODE了
            }
            else
            {
                anim.SetBool("take damage", false);
            }


        }

         float nextAttack;
         float attackRate = 2f;

        public void move()
        {

        }
        
        public void EnemyAttack() //對主角攻擊(暫時  // 之後要改由狀態機的melleattack觸發
        {
            /// 戰時能對主角進行攻擊動作 , 考慮是否將各種機器人分開寫
            /// 需要做死亡後停止觸發判定
            

            if (!Dead()|| takeDamege == false) ///
            {
                
                if (distance < 2)
                {
                    StartNewTime();
                    CountCurrentTime();
                    attack = true;
                    if(currentTime >= attackRate)
                    {
                        anim.SetBool("attack_01", true); // 做出攻擊動作  //必須要設定攻擊間隔

                        fightSystem.controllerHp -= ai_data.melleAttackD_Damage;
                        print(fightSystem.controllerHp);

                        print("Attack Controller!!");
                        
                    }                  
                }
                else
                {
                    anim.SetBool("attack_01", false);
                    attack = false;
                }
                
            }
           
            if (takeDamege == true) //承受傷害時 必須停止攻擊
            {
                attack = false;
                print("OOOOOOOOOOOOOOOO");
                anim.SetBool("attack_01", false);
                anim.SetBool("take damage", true);
            }


        }

        IEnumerator EAK()
        {
            print("AAAAA");
           yield return new WaitForSeconds(5f);
            print("BBBBB");
        }

        public void melleattack()
        {
            //if (!dead)
            //{

            //}
            ai_states.MelleAttack(); 
        }

        public void DistanceForAttack() ///計算與主角的距離
        {
            controller = GameObject.Find("Controller");
            distance =  Vector3.Distance(this.transform.position , controller.transform.position ); 
            if(distance < 10f)
            {
                transform.LookAt(controller.transform.position);
            }
            
        }

        public void Chase()
        {

        }

        public void OnTriggerEnter(Collider other) //確實執行攻擊後的觸發效果
        {
            if (other.gameObject.tag == "Weapon_point" ) /// 必須將主角武器設為"Weapon_point" !! 避免其他碰撞體傷害敵人
            {
                takeDamege = true;//觸發碰撞後,收到攻擊傷害傷害 
            }
            else
            {
                takeDamege = false;
            }

            //bug : 射擊後  和武器一同造成傷害


            if (other.gameObject.tag == "Bullet" && Input.GetKey(KeyCode.LeftControl)) /// BUG: 會一直造成損傷 就算沒有射擊  
            {
                machineDamage = true;

                //takeDamege = false;
            }
            else
            {
                machineDamage = false;
            }
            
            //TakeDamage(); //確實可執行
            //print("AAAAA");// 確實有執行
            //fightSystem.Attackking();
            //print(currentHp);
        }
        public void OnTriggerExit(Collider other) //  未 執行攻擊時 將weaponTrigger的attackTri 給關閉 //似乎邏輯不通?
        {
            takeDamege = false;
            machineDamage = false;
        }

        public float GetHealthRate()
        {
            return currentHp / maxhp;
        }
        public void StartNewTime() //開始計時, 時間為0
        {
            isTiming = true;
            currentTime = 0;
        }
        public void CountCurrentTime()//計時器開始
        {
            if (isTiming)
            {
                currentTime += Time.fixedTime;
                //print(currentTime);
            }
        }
        public void ResetTime()
        {
            if(currentTime >= 3f)
            {
                currentTime = 0;
            }
        }
    }
    
}

