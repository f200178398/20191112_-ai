﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
namespace SA
{
    public class AI_data : MonoBehaviour
    {
        public GameObject enemy;//這個怪獸
        public GameObject Target;//玩家
        [Header("=====血量=====")]
        public float maxHp;
        public float enemyDamage;
        [Header("=====距離判定=====")]
        public float targetDistance;
        public float attackDistance;
        [Header("=====近戰攻擊力=====")]
        public float melleAttackD_Damage;
        [Header("=====遠程攻擊力=====")]
        public float shoot_Damage;
        [Header("=====攻擊間隔=====")]
        public float attackRate;
        public float nextAttack;


        [Header("=====各種移動設定=====")]
        public bool bool_EnemyIsMoving;//移動狀態與否   

        public Vector3 vec_EnemyCurrentVector;//怪獸現在的向量
        public float float_EnemyCurrentMoveForce;//怪獸移動的力道
        public float float_EnemyCurrentMoveSpeed;//怪獸現在的移動速度
        public float float_EnemyMaxSpeed;//怪獸的最大移動速度

        [Header("=====各種轉向設定=====")]
        public float float_EnemyMaxRotateSpeed;//怪獸最大的轉向速度
        public float float_EnemyTempRotateForce;//怪獸暫時的轉向力道

        












        private void Awake()
        {
            //FS = GameObject.Find("FightSystem"). GetComponent<FightSystem>();
            //currentHp = maxHp;
        }


       

        void Start()
        {


           // Target = gameObject.GetComponent<EnemyTarget>().controller;



        }

        // Update is called once per frame
        void Update()
        {
            
         
            //print(m_Data.m_fHp);
            //currentHp = FS.enemyHp;
            //if (currentHp == 0)
            //{
            //    print("dead");
            //}
        }

        //int IComparable<AI_data>.CompareTo(AI_data other)   // 使用IComparable 時必須加上去
        //{
        //    throw new NotImplementedException();
        //}
    }


    public class AIFunction
    {
        //public static GameObject CheckEnemyInSight(AI_data data, ref bool bAttack)
        //{
        //    GameObject go =data. m_Target;
        //    Vector3 v = go.transform.position - data.m_Go.transform.position;
        //    float fDist = v.magnitude;
        //    if (fDist < data.m_fAttackRange)
        //    {
        //        bAttack = true;
        //        return go;
        //    }
        //    else if (fDist < data.m_fSight)
        //    {
        //        bAttack = false;
        //        return go;
        //    }
        //    return null;
        //}

        //public static bool CheckTargetEnemyInSight(AI_data data, GameObject target, ref bool bAttack)
        //{
        //    GameObject go = target;
        //    Vector3 v = go.transform.position - data.m_Go.transform.position;
        //    float fDist = v.magnitude;
        //    if (fDist < data.m_fAttackRange)
        //    {
        //        bAttack = true;
        //        return true;
        //    }
        //    else if (fDist < data.m_fSight)
        //    {
        //        bAttack = false;
        //        return true;
        //    }
        //    return false;
        //}
        public static void Move(AI_data data)
        {
            if (data.bool_EnemyIsMoving == false)//如果移動狀態=假，不做這個Move的動作
            {
                return;
            }

            Transform thisEnemyTransfrom = data.enemy.transform;//這隻怪獸的位置一開始的Trasform，先存起來
            Vector3 thisEnemyPosition = data.enemy.transform.position;//這怪獸的位置，先存起來
            Vector3 thisEnemyRight = thisEnemyTransfrom.right;//這怪獸右方
            Vector3 thisEnemyForward = thisEnemyTransfrom.forward;//這怪獸前方，用來偵測障礙物用的，什麼都沒有變過的前方先存起來
            Vector3 thisEnemyAddForceForMove = data.vec_EnemyCurrentVector;//這怪獸要移動的力量

            //下面這一串讓轉向力量太大的話不超過怪獸轉向最大速度
            if (data.float_EnemyTempRotateForce > data.float_EnemyMaxRotateSpeed)
            {
                data.float_EnemyTempRotateForce = data.float_EnemyMaxRotateSpeed;
            }
            else if (data.float_EnemyTempRotateForce < -data.float_EnemyMaxRotateSpeed)
            {
                data.float_EnemyTempRotateForce = -data.float_EnemyMaxRotateSpeed;
            }

            //下面這一串才是移動，先算出要往哪邊走
            thisEnemyAddForceForMove = thisEnemyAddForceForMove + thisEnemyRight * data.float_EnemyTempRotateForce;//總力道，包刮往前和轉彎
            thisEnemyAddForceForMove.Normalize();//把力道Normolize
            thisEnemyTransfrom.forward = thisEnemyAddForceForMove;//把移動的力量指派給往前的向量

            //下面這一串決定移動的速度
            data.float_EnemyCurrentMoveSpeed = data.float_EnemyCurrentMoveSpeed + data.float_EnemyCurrentMoveForce * Time.deltaTime;//速度=力道*Time
            if (data.float_EnemyCurrentMoveSpeed < 0.01f)//如果速度太小的話，讓他還是會移動，不要太小小到一直在計算
            {
                data.float_EnemyCurrentMoveSpeed = 0.01f;
            }
            else if (data.float_EnemyCurrentMoveSpeed > data.float_EnemyMaxSpeed)//如果超過怪獸最大速度，讓他等於最大速不要超過的意思
            {
                data.float_EnemyCurrentMoveSpeed = data.float_EnemyMaxSpeed;
            }

            //check障礙物先跳過

            //下面是算出位置(終於要移動的意思)
            thisEnemyPosition = thisEnemyPosition + thisEnemyTransfrom.forward * data.float_EnemyCurrentMoveSpeed;//位移=現在位置+往前的向量(已存Normalized後的力)
            thisEnemyTransfrom.position = thisEnemyPosition;//把新的位置告訴怪獸的Transform
        }
        static public bool Seek(AI_data data)
        {
            Vector3 cPos = data.enemy.transform.position;
            Vector3 vec = data.Target.transform.position - cPos;
            vec.y = 0.0f;
            float fDist = vec.magnitude;
            if (fDist < data.float_EnemyCurrentMoveSpeed + 0.001f)
            {
                Vector3 vFinal = data.Target.transform.position;
                vFinal.y = cPos.y;
                data.enemy.transform.position = vFinal;
                data.float_EnemyCurrentMoveForce = 0.0f;
                data.float_EnemyTempRotateForce = 0.0f;
                data.float_EnemyCurrentMoveSpeed = 0.0f;
                data.bool_EnemyIsMoving = false;
                return false;
            }
            Vector3 vf = data.enemy.transform.forward;
            Vector3 vr = data.enemy.transform.right;
            data.vec_EnemyCurrentVector = vf;
            vec.Normalize();

            float fDotF = Vector3.Dot(vf, vec);
            if (fDotF > 0.96f)
            {
                fDotF = 1.0f;
                data.vec_EnemyCurrentVector = vec;
                data.float_EnemyTempRotateForce = 0.0f;
              //  data.m_fRot = 0.0f;
            }
            else
            {
                if (fDotF < -1.0f)
                {
                    fDotF = -1.0f;
                }
                float fDotR = Vector3.Dot(vr, vec);

                if (fDotF < 0.0f)
                {

                    if (fDotR > 0.0f)
                    {
                        fDotR = 1.0f;
                    }
                    else
                    {
                        fDotR = -1.0f;
                    }

                }
                if (fDist < 3.0f)
                {
                    fDotR *= (fDist / 3.0f + 1.0f);
                }
                data.float_EnemyTempRotateForce = fDotR;

            }

            if (fDist < 10.0f)
            {
                Debug.Log(data.float_EnemyCurrentMoveSpeed);
                if (data.float_EnemyCurrentMoveSpeed > 0.1f)
                {
                    data.float_EnemyCurrentMoveForce = -fDotF * 2.0f;
                }
                else
                {
                    data.float_EnemyCurrentMoveForce = fDotF;
                }

            }
            else
            {
                data.float_EnemyCurrentMoveForce = fDotF * 100.0f;
            }



            data.bool_EnemyIsMoving = true;
            return true;
        }
        //static public bool CheckCollision(AI_data data)
        //{
        //    List<Obstacle> m_AvoidTargets = Main.m_Instance.GetObstacles();
        //    if (m_AvoidTargets == null)
        //    {
        //        return false;
        //    }
        //    Transform ct = data.m_Go.transform;
        //    Vector3 cPos = ct.position;
        //    Vector3 cForward = ct.forward;
        //    Vector3 vec;

        //    float fDist = 0.0f;
        //    float fDot = 0.0f;
        //    int iCount = m_AvoidTargets.Count;
        //    for (int i = 0; i < iCount; i++)
        //    {
        //        vec = m_AvoidTargets[i].transform.position - cPos;
        //        vec.y = 0.0f;
        //        fDist = vec.magnitude;
        //        if (fDist > data.m_fProbeLength + m_AvoidTargets[i].m_fRadius)
        //        {
        //            m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
        //            continue;
        //        }

        //        vec.Normalize();
        //        fDot = Vector3.Dot(vec, cForward);
        //        if (fDot < 0)
        //        {
        //            m_AvoidTargets[i].m_eState = Obstacle.eState.OUTSIDE_TEST;
        //            continue;
        //        }
        //        m_AvoidTargets[i].m_eState = Obstacle.eState.INSIDE_TEST;
        //        float fProjDist = fDist * fDot;
        //        float fDotDist = Mathf.Sqrt(fDist * fDist - fProjDist * fProjDist);
        //        if (fDotDist > m_AvoidTargets[i].m_fRadius + data.m_fRadius)
        //        {
        //            continue;
        //        }

        //        return true;


        //    }
        //    return false;
        //}

    }
}

