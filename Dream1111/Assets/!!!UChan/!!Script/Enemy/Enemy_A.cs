﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_A : MonoBehaviour
{

    public float enemySpeed;
    public float stoppingDistance;
    public Animator ani;
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        ani = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position , player.position)> stoppingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, enemySpeed * Time.deltaTime);
            ani.SetBool("Attack_2", true);
            Debug.Log(""+stoppingDistance);
        }
    }
}
